const request = require('request');
module.exports = async ( req, res, next ) => {
    if( req.session.auth == null && req.cookies['ADT_connect_token'] != null ){
        database.query( "SELECT * FROM users WHERE login_token = ?", [ req.cookies['ADT_connect_token'] ], ( err, results ) => {
            if( results && results.length > 0 ){
                req.session.auth = results[0];
                var options = {
                    uri: `http://api.arma-rp.fr/api/players/`+req.session.auth.steamid,
                    port: 80,
                    method: 'GET',
                }

                request( options, function( error, result, body ){
                    var json = JSON.parse( body );
                    json = json.data;
                    req.session.auth.api = json;
                    next();
                })
            } else {
                next();
            }
        })
    } else {
        if( req.session.auth != null ){
            database.query( "SELECT * FROM users WHERE id = ?", [ req.session.auth.id ], ( err, results ) => {
                if( results && results.length > 0 ){
                    req.session.auth = results[0];
                    var options = {
                        uri: `http://api.arma-rp.fr/api/players/`+req.session.auth.steamid,
                        port: 80,
                        method: 'GET',
                    }
    
                    request( options, function( error, result, body ){
                        var json = JSON.parse( body );
                        json = json.data;
                        req.session.auth.api = json;
                        next();
                    })
                } else {
                    next();
                }
            })
        } else {
            next();
        }
    }
}
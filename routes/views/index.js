module.exports.index = async ( req, res, next ) => {

    res.render( "index", {
        title: "Intranet | Accueil"
    });
};

module.exports.index_skiptuto = async ( req, res, next ) => {
    var config = JSON.parse( req.session.auth.settings );
    config.tuto = false
    config = JSON.stringify( config );
    database.query('UPDATE users SET settings = ? WHERE id = ?', [ config, req.session.auth.id ] );
    res.send("")
};
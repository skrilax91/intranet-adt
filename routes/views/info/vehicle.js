const request = require('request');

module.exports.vehicle = async ( req, res, next ) => {
    var options = {
        uri: `http://api.arma-rp.fr/api/vehicles/side/adac`,
        port: 80,
        method: 'GET',
    }

    request( options, function( error, result, body ){
        var json = JSON.parse( body );
        json = json.data;

        res.render( "info/vehicle", {
            title: "Intranet | Parc Automobile",
            vehicles: json
        });
    });

    
};
const request = require('request');
module.exports.codepenal = async ( req, res, next ) => {

    var options = {
        uri: `http://intra-gn.arma-rp.fr:4040/api/GetCode`,
        port: 80,
        method: 'GET',
    }

    request( options, function( error, result, body ){
        var json = JSON.parse( body );

        res.render( "info/codepenal", {
            title: "Intranet | Code pénal",
            code: json
        });
    });


    
};
module.exports.ban = async ( req, res, next ) => {
    if (!req.isBanned()) {
        res.redirect("/");
        return;
    }

    res.render( "ban", {
        title: "Intranet | Suspendu"
    } );
};
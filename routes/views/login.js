var steam = require('steam-login');
var randtoken = require('rand-token');
const request = require('request');
var config = require('../../config.json');

module.exports.login = [ steam.authenticate(), ( req, res, next ) => {
    res.redirect('/');
}]


module.exports.login_verify = [ steam.verify(), ( req, res, next ) => {
    
	
	if( req.isLogin() ){
        res.redirect( '/' );
        return;
    }

    if( req.isGroup(-1) ){
        res.redirect( '/' );
        return;
    }
    req.session.auth = req.user;
    var steamid = req.session.auth.steamid;

    var options = {
        uri: `http://api.arma-rp.fr/api/players/`+steamid,
        port: 80,
        method: 'GET',
    }

    request( options, function( error, result, body ){
        var json = JSON.parse( body );
        json = json.data;
        if(json.error) {
            req.session.auth = null;
            req.user = null;
            res.redirect( '/' );
            return;
        }
        if (json.adaclevel == "0") {
            req.session.auth = null;
            req.user = null;
            res.redirect( '/' );
            return;
        }

        console.log(req.session.auth.steamid);

        database.query('SELECT * FROM users WHERE steamid = ?', [ req.session.auth.steamid ], ( err, result ) => {
            if( result.length < 1 ){
                console.log("test");
                var avatar = req.session.auth.avatar.large
                database.query('INSERT INTO users( servid, steamid, avatar, join_at, settings) VALUES( ?, ?, ?, NOW(), ?)', [ 0, steamid, avatar, JSON.stringify(config.defaultSettings)], ( err, result_ ) => {
                    if (err) {
                        console.log(err);
                    }
                })
                req.session.auth = null;
                req.user = null;
                req.flash('success', "Config créés, veuillez vous reconnecter");
                res.redirect( '/' );
                
                return;
            }

            if (err) {
                console.log(err);
            }

            var token = randtoken.generate(200);
            console.log(result.length)
            if( result.length < 1 ){
                req.session.auth = null;
                req.user = null;
                req.flash('error', "Erreur de connexion");
                res.redirect( '/' );
                return;
            }

            req.session.auth = result[0];

            var expires = new Date();
            expires.setTime( expires.getTime() + ( 365 * 24 * 60 * 60 * 1000 ) );

            res.cookie( "ADT_connect_token", token, { expires: expires }, );

            database.query('UPDATE users SET login_token = ? WHERE id = ?', [ token, req.session.auth.id ] );

            req.flash('success', "Vous êtes bien connecté");
            res.redirect( '/' );
            return;
        })

    });

    
}];

module.exports.logout = ( req, res, next ) => {
    res.clearCookie( "ADT_connect_token" ); 
    req.session.auth = null;
    req.user = null;

    res.redirect('/');
};
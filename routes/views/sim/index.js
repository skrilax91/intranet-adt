module.exports.sim_index = async ( req, res, next ) => {

    res.render( "sim/index", {
        title: "Intranet | Service Interne de Messagerie",
    } );
};
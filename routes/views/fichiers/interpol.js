const request = require('request');
module.exports.fichiers_interpol = async ( req, res, next ) => {

    var options = {
        uri: `http://intra-gn.arma-rp.fr:4040/api/GetInterpol/`,
        port: 80,
        method: 'GET',
    }

    request( options, function( error, result, body ){
        var json = JSON.parse( body );
        

        res.render( "fichiers/interpol", {
            title: "Intranet | Interpol",
            peines: json,
        } );
    });

    
};
module.exports.cgr_create = async ( req, res, next ) => {

    res.render( "cgr/create", {
        title: "Intranet | Rédaction"
    });
};

module.exports.cgr_add = async ( req, res, next ) => {
    var title = req.body.title;
    var content = req.body.content;
    var date = req.body.date;

    if( title.length > 100 || title.length < 10 ){
        req.flash('error', "Titre invalide");
        res.reload();
        return;
    }
    if( !app.validPost(Date.parse(date)) ){
        req.flash('error', "Date invalide");
        res.reload();
        return;
    }

    database.query('INSERT INTO CGR(CGR_authid, CGR_dest, CGR_title, CGR_content, CGR_date) VALUES(?,?,?,?,?,?)', [req.session.auth.id, "", title, content, date], ( err, result ) => {
        if( err ) throw err;

        req.flash('success', "Rapport créer avec succès.");
        res.redirect('/cgr/');
    })
    

};
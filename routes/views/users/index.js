const request = require('request');
module.exports.users_index = async ( req, res, next ) => {
    var users = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM users', ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });

    var options = {
        uri: `http://api.arma-rp.fr/api/players/`,
        port: 80,
        method: 'GET',
    }

    request( options, function( error, result, body ){
        var json = JSON.parse( body );
        json = json.data;
        var vh = {};
        var arrayLength = 0;
        for(var k in json){
            
            var tbl = json[k];
            if (tbl.adaclevel > 0){

                arrayLength = Object.keys(vh).length;
                vh[ arrayLength ] = tbl;
            }
        }
        

        res.render( "users/index", {
            title: "Intranet | Effectifs",
            users: vh
        });
    });

    
};
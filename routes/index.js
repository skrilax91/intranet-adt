var express = require('express');
var router = express.Router();

const { catchErrors } = require('../class/errorHandlers.js');
const { permissions } = require('../class/permissions.js');
var { maintenance } = require('./views/maintenance');
var { vehicle } = require('./views/info/vehicle');
var { codepenal } = require('./views/info/codepenal');
var { reglementinstit } = require('./views/info/reglement');
var { ban } = require('./views/ban');
var { index } = require('./views/index');
var { login, login_verify, logout } = require('./views/login');
var { users_index} = require('./views/users/index');
var {
	users_profil,
	users_changeperms,
	users_delete,
	users_admin,
	users_suspend
} = require('./views/users/profil');
var { fichiers_interpol } = require('./views/fichiers/interpol');
var { sim_index } = require('./views/sim/index');
var { cgr_index } = require('./views/cgr/index');
var { 
	cgr_create,
	cgr_add
} = require('./views/cgr/create');
var { cgr_view } = require('./views/cgr/view');

// Homes pages
router.get( '/', catchErrors( index ) );
router.get( '/maintenance/', catchErrors( maintenance ) );

// Login pages
router.get( '/login/', login );
router.get( '/verify/', login_verify );
router.get( '/logout/', logout );

// Register pages
router.get( '/ban/', catchErrors( ban ));

// info pages
router.get( '/info/vehicle', permissions(1), catchErrors( vehicle ));
router.get( '/info/reglement', permissions(1), catchErrors( reglementinstit ));
router.get( '/info/code', permissions(1), catchErrors( codepenal ));

// users pages
router.get( '/users/', permissions(1), catchErrors( users_index ));
router.get( '/users/profil/:id', permissions(1), catchErrors( users_profil ));
router.post( '/users/profil/:id/changeperms', permissions(1), catchErrors( users_changeperms ));
router.get( '/users/profil/:id/delete', permissions(1), catchErrors( users_delete ));
router.get( '/users/profil/:id/suspend/:state', permissions(1), catchErrors( users_suspend ));
router.get( '/users/profil/:id/admin/:state', permissions(1), catchErrors( users_admin ));


router.get( '/fichiers/interpol', permissions(1), catchErrors( fichiers_interpol ));

// S.I.M pages
router.get( '/sim/', permissions(1), catchErrors( sim_index ));

// C.G.R pages
router.get( '/cgr/', permissions(1), catchErrors( cgr_index ));
router.get( '/cgr/create', permissions(1), catchErrors( cgr_create ));
router.post( '/cgr/add', permissions(1), catchErrors( cgr_add ));
router.get( '/cgr/view/:id', permissions(1), catchErrors( cgr_view ));

module.exports = router;